<?php

namespace App\Http\Controllers;

use App\Pariwisata;
use Illuminate\Http\Request;

use Storage;
use Carbon\Carbon;

class PariwisataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pariwisata = Pariwisata::orderBy('created_at', 'desc')->get();
        return view('index', compact('pariwisata'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'pariwisata_nama' => ['required', 'string'],
            'pariwisata_alamat' => ['required', 'string'],
            'pariwisata_detail' => ['required', 'string'],
            'pariwisata_gambar' => ['required', 'mimes:jpg,jpeg,png'],
        ]);

        $gambar = $this->imageUpload($request);
        $store = Pariwisata::create([
            'pariwisata_nama' => $request->pariwisata_nama,
            'pariwisata_alamat' => $request->pariwisata_alamat,
            'pariwisata_detail' => $request->pariwisata_detail,
            'pariwisata_gambar' => $gambar,
        ]);

        return redirect('/basic');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pariwisata  $pariwisata
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pariwisata  $pariwisata
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        return view('edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pariwisata  $pariwisata
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pariwisata  $pariwisata
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Upload Files to App
     * 
     */
    private function imageUpload($request, $location = 'img')
    {
        $uploadedFile = $request->file('pariwisata_gambar');        
        $filename = strtolower(str_replace(' ', '_', $request->pariwisata_nama)).'-'.(Carbon::now()->timestamp+rand(1,1000));
        $path = $uploadedFile->storeAs($location, $filename.'.'.$uploadedFile->getClientOriginalExtension());
        
        return $filename.'.'.$uploadedFile->getClientOriginalExtension();
    }
}
