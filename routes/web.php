<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function(){
    return view('welcome');
});
Route::resource('/basic', 'PariwisataController');

Route::group([
    'prefix' => 'ajax'
], function(){
    Route::get('/', function () {
        return view('ajax.index');
    });
    Route::get('/pariwisata', 'AJAX\PariwisataController@index');
    Route::post('/pariwisata', 'AJAX\PariwisataController@store');
    Route::get('/pariwisata/{id}', 'AJAX\PariwisataController@show');
    Route::put('/pariwisata/{id}', 'AJAX\PariwisataController@update');
    Route::delete('/pariwisata/{id}', 'AJAX\PariwisataController@destroy');
});